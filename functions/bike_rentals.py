"""

This module has one class (BikeRentals) that gets a zip file of the Bike Sharing Dataset,
unzips it and creates a pandas dataframe. It can also create a correlation matrix and
a regression out of the data. The class and all methods are explained in further detail below.

"""


import zipfile
from datetime import datetime
from pathlib import Path
import requests
import numpy as np
import pandas as pd
import seaborn as sns


import matplotlib.pyplot as plt


class BikeRentals:
    """
    A class that allows us to get the zipfile from the url with
    bike rentals data and perform analysis on it

    ...

    Attributes
    ----------
    self.data_df : pandas.core.frame.DataFrame
        a pandas dataframe from the specified csv, default hours.csv
    self.url : str
        url where Bike-Sharing-Dataset.zip file can be downloaded

    Methods
    -------
    download_url(save_path=Path("../downloads/data_zip_file.zip"), chunk_size=128)
        Downloads zip from url given by professor

    unzip(save_path=Path("../downloads/data_zip_file.zip"), csv_name="hour.csv")
        Unzips previously created zip document.
        It also turns the index into the day in
        datetime format and changes the weekday
        column to the European weekday format.

    correlation_matrix()
        Builds the correlation matrix with the respective columns and returns it

    weekly_plot()
        The user chooses a week and inputs it. The function
        builds a simple plot, with column 'instant' in the x-axis
        and the column 'cnt' in the y-axis for the chosen week.
        Thus, the user can see the number of bikes rented in each
        instant for the selected week.

    avg_rent_month()
        Builds a bar plot of the average total rentals by month
        of the year.

    forecast()
        This method plots the expected bike rentals per hour
        and weekday, based on the mean of the rentals for those
        periods in the previous years, as well as a shaded area
        corresponding to an interval of [-1 std deviation, +1 std deviation.]

    """

    def __init__(self):
        """Initialize the variables needed for the rest of the class methods"""

        self.data_df = pd.DataFrame
        self.url = (
            "https://archive.ics.uci.edu/ml/machine-learning-databases/"
            "00275/Bike-Sharing-Dataset.zip"
        )

    def download_url(
        self, save_path=Path("../downloads/data_zip_file.zip"), chunk_size=128
    ):
        """Downloads zip from url given by professor"""

        if save_path.is_file():
            raise Exception("File already exists")

        requested_url = requests.get(self.url, stream=True)
        with open(save_path, "wb") as file_open:
            for chunk in requested_url.iter_content(chunk_size=chunk_size):
                file_open.write(chunk)

    def unzip(
        self, save_path=Path("../downloads/data_zip_file.zip"), csv_name="hour.csv"
    ):
        """
        Unzips previously created zip document.
        It also turns the index into the day in
        datetime format and changes the weekday
        column to the European weekday format.
        """

        zipped_file = zipfile.ZipFile(save_path)
        self.data_df = pd.read_csv(zipped_file.open(csv_name))
        pd.options.mode.chained_assignment = None


        self.data_df.set_index(
            pd.to_datetime(self.data_df.dteday) + self.data_df.hr.astype("timedelta64[h]"),
            inplace=True,
        )

        for num in range((len(self.data_df))):
            self.data_df['weekday'][num] = self.data_df.index[num].weekday()

    def correlation_matrix(self):
        """Builds the correlation matrix with the respective columns and returns it

        Returns:
        -------

            plt.show(): returns plt graph that is a correlation matrix between
            some of the relevant variables in the dataset.
        """

        df_corr = self.data_df[
            ["mnth", "hum", "weathersit", "temp", "windspeed", "cnt"]
        ]
        corr_matrix = df_corr.corr()
        plt.subplots(figsize=(12, 8))

        mask = np.triu(np.ones_like(corr_matrix, dtype=bool))

        # Generate a custom diverging colormap
        cmap = sns.diverging_palette(200, 20, as_cmap=True)

        # Draw the heatmap with the mask and correct aspect ratio
        sns.heatmap(
            corr_matrix,
            mask=mask,
            cmap=cmap,
            annot=True,
            vmax=0.3,
            center=0,
            square=True,
            linewidths=0.5,
            cbar_kws={"shrink": 0.5},
        )

        plt.title("\nCorrelation Matrix", {"fontsize": 16})

        return plt.show()

    def weekly_plot(self):
        """The user chooses a week and inputs it. The function
        builds a simple plot, with column 'instant' in the x-axis
        and the column 'cnt' in the y-axis for the chosen week.
        Thus, the user can see the number of bikes rented in each
        instant for the selected week.


        Returns:
        -------

                plt.show(): returns plt graph that is a scatter plot of
                the number of rentals and the instant in time for the
                chosen week.
        """
        pd.options.mode.chained_assignment = None

        week_exclude = [0, 96, 105]

        rentals_df = self.data_df

        rentals_df["week"] = 0

        for num in range((len(rentals_df) - 1)):
            if rentals_df.index[num].week != rentals_df.index[num + 1].week:
                rentals_df["week"][num + 1] = rentals_df["week"][num] + 1
            else:
                rentals_df["week"][num + 1] = rentals_df["week"][num]

        for week_to_exclude in week_exclude:
            rentals_df = rentals_df[rentals_df["week"] != week_to_exclude]

        keys = list(set(list(rentals_df["week"])))

        new_values = list(range(103))

        dic_change = dict(zip(keys, new_values))

        # rentals_df = rentals_df.replace({"week": dic_change})
        rentals_df["week"] = rentals_df["week"].map(dic_change)

        week_num = int(input("Choose the week you want to see the data for:"))

        if week_num not in range(103):
            raise Exception("Please enter a number between 0 and 102 :)")

        filtered_week_df = rentals_df[rentals_df["week"] == week_num]

        filtered_week_df.plot(y="cnt", use_index=True, figsize=(15, 6), style="o")

        plt.title(
            "\nNumber of bike rentals per instant (for week {0})\n".format(week_num),
            {"fontsize": 16},
        )

        return plt.show()

    def avg_rent_month(self):
        """Builds a bar chart of the average total rentals by month
        of the year.

        Returns:
        -------
                plt.show(): returns a bar chart of the average total
                rentals by month
                of the year.
        """

        hours_df = self.data_df

        hours_df["week"] = 0

        for num in range((len(hours_df) - 1)):
            if hours_df.index[num].week != hours_df.index[num + 1].week:
                hours_df["week"][num + 1] = hours_df["week"][num] + 1
            else:
                hours_df["week"][num + 1] = hours_df["week"][num]

        (hours_df.groupby("mnth")["cnt"].agg(["sum"]) / 2).plot.bar(figsize=(14, 7))
        for i in range(12):
            plt.text(
                i,
                (hours_df.groupby("mnth")["cnt"].agg(["sum"]) / 2)["sum"][i + 1],
                f"{int((hours_df.groupby('mnth')['cnt'].agg(['sum'])/2)['sum'][i+1]):,}",
                horizontalalignment="center",
                fontsize=11,
                verticalalignment="bottom",
            )
        plt.xlabel("Month")
        plt.legend(["Monthly Average"])
        plt.title("\nMonthly Average\n", {"fontsize": 16})

        return plt.show()

    def forecast(self):
        """This method plots the expected bike rentals per hour
        and weekday, based on the mean of the rentals for those
        periods in the previous years, as well as a shaded area
        corresponding to an interval of [-1 std deviation, +1 std deviation.]

        Returns:
        -------
                plt.show(): returns a bar chart of the
                expected number of bike rentals for each hour and weekday
                of a certain month.

        """

        inp = input("Choose the month you want to see the data for:")

        try:
            if inp.isdigit():
                mnth_num = int(inp)
            else:
                mnth_inp = datetime.strptime(inp, "%B")
                mnth_num = int(mnth_inp.strftime("%m"))

            hours_df = self.data_df

            metrics_df = (
                hours_df.groupby(["mnth", "weekday", "hr"])["cnt"]
                .agg(["mean", "std"])
                .reset_index(level=[0, 1, 2])
            )

            metrics_df = metrics_df[metrics_df["mnth"] == mnth_num]

            dummy, axis = plt.subplots(figsize=(30, 12))

            axis.fill_between(
                metrics_df.index,
                metrics_df["mean"] - metrics_df["std"],
                metrics_df["mean"] + metrics_df["std"],
                color="teal",
                alpha=0.2,
                label="Standard Deviation",
            )

            metrics_df["mean"].plot(ax=axis, c="C3", label="Expected bike rentals")

            axis.set_ylabel("Expected bike rentals", fontsize=13)
            axis.set_xlabel("\n\nHour\nWeekday", fontsize=13)
            axis.set_xticks(list(metrics_df.index))
            axis.set_xticklabels(metrics_df["hr"])
            for row in range(metrics_df.index[0], metrics_df.index[-1]):
                if metrics_df["weekday"][row] != metrics_df["weekday"][row + 1]:
                    plt.axvline(x=row, ls=":", c="b", label="_nolegend_")
                    plt.text(
                        row,
                        metrics_df["mean"][row],
                        metrics_df["weekday"][row + 1],
                        horizontalalignment="left",
                        fontsize=16,
                        position=(row, -70),
                    )

            plt.ylim((0, 1200))
            plt.xlim((metrics_df.index[0], metrics_df.index[-1]))
            plt.title(
                "\nExpected weekly rentals in {0}\n".format(
                    datetime.strptime(str(mnth_num), "%m").strftime("%B")
                ),
                {"fontsize": 20},
            )
            plt.tight_layout()
            plt.legend()
            plt.show()

        except (ValueError, IndexError):
            print("Invalid input. Please insert the month name or number :)")
