Team members:
- Ângelo Figueiral, 44645@novasbe.pt
- Diogo Cardoso, 45124@novasbe.pt
- Pedro Rolim, 29067@novasbe.pt
- Ricardo Araújo, 32025@novasbe.pt

This repository contains the work developed by the team members as part of the two-day Cycling Mobility Hackathon. This is an effort to contribute to the green transition by promoting cycling awareness. The information source is a Bike Sharing dataset from Capital Bikeshare system, Washington D.C., USA, based on the years 2011 and 2012. Our analysis and main findings can be found on the showcase notebook. 

In the first day we analyzed the relationship between the number of bike rentals and some variables that might be correlated with it, such as weather condition variables. In the second day we added some additional features in order to complement our findings, such as predictive capabilities and an evaluation of bicycle rental frequency throughout time, in order to assess the needs of bycicle availability at different periods and increase efficiency.
